# 1. Premier graphique.

[Le site en question](https://www.le-diabete-dans-tous-ses-etats.precidiab.org/diabete-de-type-2/etats-unis-une-insuline-hors-de-prix/)

Nous pouvons voir que l'hypertension, l'indice de masse corporelle et le taux de glucose dans le sang sont en corrélation et montrent très clairement qu'ils sont à prendre en compte avec le diabète.

Le Hb1ac est en dessous de la moyenne pour les diabétiques présents dans la base de données car le taux est de 7%. Le plus dur étant de voir que ceux qui sont supérieurs à 7% risquent de développer des complications liés au diabète.

Indice de masse corporelle: Les données présentées nous montrent que l'indice de masse corporelle est supérieur à 30 kg/m2 ce qui est dangereux pour la santé des patients.

Fumeurs et non fumeurs: selon les données, les fumeurs sont les plus nombreux et les diabétiques ont plus de chance d'avoir le diabète comme le prouve cette étude.

Il y a plus de femmes qui sont diabétiques que d'hommes selon les données et selon une autres étude, il y a plus de femmes diabétiques que d'hommes diabétiques.

[Hommes et femmes diabétiques](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5579421/)

[Fumeurs](https://www.ameli.fr/assure/sante/themes/diabete/diabete-interlocuteurs/diabete-et-tabac)

[IMC](https://www.santemagazine.fr/actualites/actualites-sante/lindice-de-masse-corporelle-est-un-facteur-de-risque-de-diabete-plus-puissant-que-la-genetique-852278)

[Le Hb1ac](https://www.federationdesdiabetiques.org/information/glycemie/hba1c)

[Les données du site](https://www.biofrance.synlab.fr/Media/PDF/BIOFRANCE/guide_automesure_diabete_patient.pdf)

[Définition du diabète](https://www.ameli.fr/assure/sante/themes/diabete/diabete-comprendre/definition)

## 2. Deuxième graphique.

Le diabète chez les femmes enceintes et les enfants est en corrélation comme le montre ce lien:

[Femmes enceintes](https://www.federationdesdiabetiques.org/information/diabete-gestationnel)

La pression artérielle est un facteur corroborant du diabète même si le taux selon le graphique est faible alors que c'est un facteur aggravant du diabète.

Dans ce cas précis, la pression artérielle selon les données proposées sont bonnes pour le taux en vigueur.

[Pression artérielle](https://www.diabete.qc.ca/le-diabete/informations-sur-le-diabete/maladies-associees/le-controle-de-la-tension-arterielle/)

L'épaisseur de la peau et la pression artérielle ont des valeurs concommitantes et est dans les standards moyens de la maladie.

[Epaisseur de la peau](https://www.bd.com/resource.aspx?IDX=25838)

Le glucose se trouve être dans la moyenne et il est malgré tout inquiétant.

[Glucose et glycémie](https://www.diabete.fr/quel-est-le-bon-taux-de-glycemie-jeun/)

L'insuline est normale pour les patients présents au sein de la base de données.

[Insuline](https://www.federationdesdiabetiques.org/information/glycemie)

Age: le diabète se développe très jeune et forte majorité chez les jeunes adultes.

[Age](https://www.msdmanuals.com/fr/professional/p%C3%A9diatrie/pathologies-endocriniennes-p%C3%A9diatriques/le-diab%C3%A8te-sucr%C3%A9-chez-les-enfants-et-les-adolescents)

Résultat du diabète.

Selon les données proposées il y a une majorité de personnes qui n'ont pas le diabète et une minorité qui ont le diabète.

### 3. Troisième graphique.

Le cholestérol et les fumeurs sont un facteur corroborant du diabète.

[Cholestérol et fumeurs](https://www.dinnosante.fr/gestion-diabete/suivi-medical-et-soins/cholesterol)

HDL: plus le taux est moyen, plus les risques pour la santé des diabétiques est moindre.

[HDL](https://www.ameli.fr/assure/sante/themes/diabete-adulte/diabete-suivi/analyses-sang-urines)

GLYHB: le graphique montre que les données sont inférieures à la moyenne.

[GLYHB](https://www.federationdesdiabetiques.org/information/glycemie/hba1c)

L'âge est en corrélation avec le diabète car on découvre cette maladie aux alentours des 15 ans et des 19 ans.

Lien précédent vers l'âge.

Poids et diabète sont liés.

[Poids et diabète](https://www.santemagazine.fr/sante/maladies/maladies-endocriniennes-et-metaboliques/obesite/quand-le-surpoids-conduit-au-diabete-de-type-2-177950)
